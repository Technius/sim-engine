package co.technius.gameengine.dsl

import co.technius.gameengine._

abstract class SimpleSimulation extends Simulation {

  import models._

  def simOutOps: SimOutputOps[models.type]

  def run(systems: List[System])(world: World, entities: EntityMap): EntityMap = {
    val output = generateOutput(systems, world, entities)
    processOutput(entities, output)
  }

  def generateOutput(systems: List[System], world: World, entities: EntityMap): SimOutput = {
    systems.aggregate(simOutOps.empty)((acc, s) =>
      simOutOps.combine(acc, s(entities, world)), simOutOps.combine)
  }
}
