name := """sim-engine"""

version := "1.0"

scalaVersion in ThisBuild := "2.12.0"

lazy val root = (project in file("root")).aggregate(core, macroDsl, example)

lazy val core = (project in file("core"))

lazy val macroDsl =
  (project in file("dsl"))
    .settings(
      libraryDependencies ++= Seq(
        "org.scala-lang" % "scala-reflect" % scalaVersion.value % "compile"
      )
    )
    .dependsOn(core)

lazy val example = (project in file("example")).dependsOn(core, macroDsl)

// Change this to another test framework if you prefer
libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.0" % "test"

// Uncomment to use Akka
//libraryDependencies += "com.typesafe.akka" %% "akka-actor" % "2.3.11"

