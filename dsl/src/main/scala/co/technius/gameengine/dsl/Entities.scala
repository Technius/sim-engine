package co.technius.gameengine.dsl

import scala.language.experimental.macros
import scala.reflect.macros.blackbox.Context

import co.technius.gameengine._

object Entities {
  private[this] type _Entity[M <: Models] = (M#Id, M#ComponentSeq)

  implicit class EntityOps[M <: Models](val self: _Entity[M]) extends AnyVal {
    def id: M#Id = self._1
    def components: M#ComponentSeq = self._2
  }

  implicit class ComponentSeqOps[M <: Models](val self: M#ComponentSeq) extends AnyVal {
    def componentOf[A <: M#Component]: Option[A] = macro Impl.componentOf[A]
    def replace[A <: M#Component](value: A): Unit = macro Impl.replace[A]
  }

  object Impl {
    def underlying(c: Context) = {
      import c.universe._
      val c.Expr(q"$_[$_]($thisRef)") = c.prefix
      thisRef
    }
    def componentOf[T: c.WeakTypeTag](c: Context): c.Expr[Option[T]] = {
      import c.universe._
      val typ = weakTypeOf[T]
      val tree = q"${underlying(c)}.collectFirst { case x: $typ => x }"
      c.Expr[Option[T]](tree)
    }

    def replace[T: c.WeakTypeTag](c: Context)(value: c.Expr[T]): c.Expr[Unit] = {
      import c.universe._
      val typ = weakTypeOf[T]
      val tree = q"""
        var i = 0
        val it = ${underlying(c)}.iterator
        var continuing = true
        while (it.hasNext && continuing) {
          val n = it.next()
          if (n.isInstanceOf[$typ]) {
            ${underlying(c)}(i) = $value
            continuing = false
          }
          i = i + 1
        }
      """
      c.Expr[Unit](tree)
    }
  }
}
