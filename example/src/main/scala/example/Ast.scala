package example

import Models._

sealed trait ComponentAst
case class StringComponent(value: String) extends ComponentAst
case class NumberComponent(value: Int) extends ComponentAst

sealed trait EventAst
case class SetStringEvent(id: Id, newValue: String) extends EventAst
case class SpawnIncrementEvent(maxValue: Int) extends EventAst

final case class WorldAst(something: Int)
