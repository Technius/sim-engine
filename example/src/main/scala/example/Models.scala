package example

import co.technius.gameengine
import scala.collection.immutable.{ Map, Seq }

object Models extends gameengine.Models {
  type Id = Int
  type Component = ComponentAst
  type Event = EventAst
  type SimOutput = Seq[Event]
  type World = WorldAst
}
