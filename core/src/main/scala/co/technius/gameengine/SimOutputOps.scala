package co.technius.gameengine

trait SimOutputOps[M <: Models] {

  def combine(a: M#SimOutput, b: M#SimOutput): M#SimOutput

  def empty: M#SimOutput
}
