package example

import co.technius.gameengine
import co.technius.gameengine.dsl.Entities._

import Models._

final class Simulation extends gameengine.dsl.SimpleSimulation {

  val models = Models

  val simOutOps = SimOutputOps

  def processOutput(entities: EntityMap, output: SimOutput): EntityMap = {
    output foreach {
      case SetStringEvent(id, v) =>
        entities.get(id).foreach(_.replace(StringComponent(v)))
      case SpawnIncrementEvent(max) =>
        for {
          (id, components) <- entities
          nc <- components.componentOf[NumberComponent] if nc.value == max
        } {
          NumberComponent(nc.value + 1) +=: components
        }
      case _ =>
    }
    entities
  }
}
