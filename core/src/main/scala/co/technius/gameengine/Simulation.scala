package co.technius.gameengine

import scala.collection.immutable.Seq

/*
 * @tparam M The trait defining the models
 * @tparam W The world type
 */
abstract class Simulation {

  val models: Models

  import models._

  def run(systems: List[System])(world: World, entities: EntityMap): EntityMap

  def generateOutput(systems: List[System], world: World, entities: EntityMap): SimOutput

  def processOutput(entities: EntityMap, output: SimOutput): EntityMap
}
