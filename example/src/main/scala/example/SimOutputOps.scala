package example

import co.technius.gameengine
import scala.collection.immutable.Seq

import Models._

object SimOutputOps extends gameengine.SimOutputOps[Models.type] {

  def combine(a: SimOutput, b: SimOutput) = a ++ b

  def empty = Seq.empty[Event]
}
