package example

import scala.collection.mutable.{ ListBuffer, Map => MutMap }

import co.technius.gameengine.dsl.Entities._

import Models._

object ExampleApp {
  def main(args: Array[String]): Unit = {
    val sim = new Simulation
    val systems = List[System](
      (e, w) => e.map(ent => SetStringEvent(ent.id, "test")).toList,
      (e, w) => {
        val nums = for {
          comps <- e.values
          n <- comps.componentOf[NumberComponent]
        } yield n.value
        if (nums.size == 0) List()
        else List(SpawnIncrementEvent(nums.max))
      }
    )
    val world = WorldAst(0)
    val entities = entityMap(
      1 -> componentSeq(StringComponent("yo")),
      2 -> componentSeq(NumberComponent(1))
    )
    println("Before: " + entities.toString)
    sim.run(systems)(world, entities)
    println("1 tick: " + entities.toString)
  }
}
