package co.technius.gameengine

import scala.collection.immutable.{ Map, Seq }
import scala.collection.mutable.{ ListBuffer, Map => MutMap}

/**
 * Defines base data types to use in the simulation.
 */
trait Models {

  /**
   * The type of identifier to use
   */
  type Id

  /**
   * The type of component
   */
  type Component

  /**
   * Shorthand for a mutable component collection
   */
  type ComponentSeq = ListBuffer[Component]

  /**
   * A component collection associated with an Id
   */
  type Entity = (Id, ComponentSeq)
  
  /**
   * A collection of entities
   */
  type EntityMap = MutMap[Id, ComponentSeq]

  /**
   * Represents the data output by a simulation
   */
  type SimOutput

  /**
   * Represents global data state
   */
  type World

  /**
   * Represents a component system
   */
  type System = (EntityMap, World) => SimOutput
  
  /**
   * Creates an [[EntityMap]] containing the specified entities.
   */
  def entityMap(entities: Entity*): EntityMap = MutMap(entities: _*)

  /**
   * Creates a [[ComponentSeq]] containing the specified components.
   */
  def componentSeq(components: Component*): ComponentSeq = ListBuffer(components: _*)

  /**
   * Provides an implicit reference to this type for use in DSLs.
   */
  implicit def modelRef: this.type = this
}
